#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
    Name:
        Main.py

    Purpose:
        setup and start service in project

    Last Updated:
        30/set/2019

    Author:
        Carlos Fernando Chicata Farfan
"""
from flask import Flask
from flask_socketio import SocketIO

app = Flask(__name__)
socketio = SocketIO(app)


if __name__ == '__main__':
    socketio.run()


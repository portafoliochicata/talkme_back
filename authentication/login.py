#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
    Name:
        Login.py

    Purpose:
        Contain all methods and logistic to authenticate users

    Last updated:
        28/set/2019

    Author:
        Carlos Chicata Farfan
"""
from pymongo import MongoClient
from werkzeug.security import generate_password_hash
from flask_restful import Resource, reqparse
from flask import request, jsonify

from access_db import manage_api_db


parser = reqparse.RequestParser()
DB_ACCESS = manage_api_db(
    api='mongo',
    url="mongodb://localhost:27017/",
    name_db="TalkMe"
)


class Authentication(Resource):
    """
    manage all methods to authentication
    """
    def post(self):
        # format json response
        json_response = {
            'error': {
                'code': 0,
                'message': '',
            },
            'token': ''
        }
        # parsing data
        parser.add_argument('username', type=str, default=None)
        parser.add_argument('password', type=str, default=None)
        args = parser.parse_args()
        # case 1: missing params in json
        if args['username'] is None or args['password'] is None:
            json_response['error']['code'] = 1
            json_response['error']['message'] = "Missing password or username"
            return json_response, 400
        # case 2: verification connection with server
        if DB_ACCESS is None:
            json_response['error']['code'] = 2
            json_response['error']['message'] = "There is not connection in server."
            return json_response
        # case 3: there are more one user with same credentials or anything
        collection = DB_ACCESS['TalkUser']
        user = collection.find({'nickname' : 'CarlosCh'})
        if user.count() > 1:
            json_response['error']['code'] = 3
            json_response['error']['message'] = "there are many users with same credentials."
            return json_response, 400
        # case 4: there are not anything user with same credentionals
        if user.count() == 0:
            json_response['error']['code'] = 4
            json_response['error']['message'] = "There is not user with same credentials."
            return json_response, 400
        # case 0: success login
        json_response['token'] = user[0]['token']
        return json_response, 200

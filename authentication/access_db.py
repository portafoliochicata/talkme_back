#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
    Name:
        access_db.py

    Purpose:
        Contain all methods and logistic to retrieve data from db.

    DB support:
        - MongoDB 4.2

    Last updated:
        28/set/2019

    Author:
        Carlos Chicata Farfan
"""
from abc import ABC, abstractmethod

from pymongo.errors import ConnectionFailure
from pymongo import MongoClient


class AbstractAccessDB(ABC):
    """
    Abstract class to define all other class to access to databases
    :param url: string of url to access database
    """
    def __init__(self, url):
        """
        build a instance of interface to access database
        :param url: string; url to access database
        """
        self.url = url

    def connection(self):
        """
        connect to database
        :return: return status of connection
        """
        pass

    def get_db(self, name):
        """
        get specified db
        :param name: string; name of DB
        :return: return instance of data
        """
        pass


class MongoDB(AbstractAccessDB):
    """
    manage access to MongoDB
    :param connection: instance of mongodb API
    """
    def __init__(self, url):
        """
        build mongodb instance
        :param url: string to access mongodb
        """
        super().__init__(url)
        self.__connection = None
        self.__status_server = True

    def connection(self):
        """
        get a connection with mongodb instance
        :return: status of connection
        """
        try:
            self.__connection = MongoClient(self.url)
        except ConnectionFailure:
            self.__status_server = False

    def get_db(self, name):
        """
        implementation of get_db
        :param name: string; name of db
        :return: access to db
        """
        if self.__status_server is True:
            return self.__connection[name]
        else:
            return None


def manage_api_db(api='mongo', url="", name_db=""):
    """
    select a specified instance of DB API
    :param api: specify instance of DB API
    :param url: direction of DB
    :return: instance of mongo
    """
    if api == 'mongo':
        instance = MongoDB(url)
        instance.connection()
        return instance.get_db(name_db)
    raise Exception("This option of API DB isn't exist")

